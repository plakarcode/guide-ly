// 'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {


	//Functions

		//Global variables
		var windowHeight = $(window).height(),
			windowWidth = $(window).width(),
			headerHeight = $('.header-main').outerHeight(),
			asideHeight = $('.aside').outerHeight();

		var isChrome = !!window.chrome && !isOpera;
		var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;



		//Fullscreen wrap
		function fullHeight(elem) {

			var fullScreenElement = $('.'+ elem);


			if(windowWidth > 769) {
				fullScreenElement.css({
					height: windowHeight + 'px'
				});
			} else {

				fullScreenElement.css({
					height: 'auto'
				});
			}
		}

		//Set image element as background image of his parent
		function setBgr(elem) {

			$('.' + elem).each(function(){
			      var src = $(this).find('.bgr-img').attr('src');
			      $(this).css('backgroundImage', 'url('+ src +')');
			      $(this).find('.bgr-img').hide();
			});
		}

		function calcTopMargin(elem) {
			$('.' + elem).css({
				marginTop: -headerHeight + 'px',
				marginBottom: -headerHeight + 'px'
			});
		}

	//Activate mobile menu
	$('.js-menu').on('click', function(){
		$('body').toggleClass('menu-is-active');
	});
	//Close mobile menu
	$('.js-close-menu').on('click', function(){
		$('body').removeClass('menu-is-active');
	});

	//Dropdown
	$('.select').selectOrDie({
		 customClass: "no_highlight"
	});


	//Sticky aside
	if($('.aside').length) {
		$('.aside').hcSticky();
	}

	//SCROLL TO ELEMENT ON CLICK ON LINK
	var $root = $('html, body');

    $('a[href*=#]').click(function() {

        var href = $(this).attr('href');

        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

            if (target.length) {
                $root.animate({
                    scrollTop: target.offset().top  + 30
                }, 500);
                return false;
            }
        }
    });

	//Fullscreen
	fullHeight('full-screen');
	fullHeight('item');
	fullHeight('full__content');
	fullHeight('home-map');
	fullHeight('map');
	// fullHeight('aside');

	//Screenshoots slider
	$('.slider--screenshot').owlCarousel({
	    // loop:true,
	    margin:40,
	    nav:true,
	    autoheight: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        769:{
	            items:2
	        },
	        900:{
	            items:3
	        },
	        1300:{
	            items:4
	        }
	    },
	    navText: [
	          "<svg role='img' class='icon color-text icon--43-24 '>
	              <use xlink:href='icons/icons.svg#icon-arrow-left-big'></use>
	            </svg>",
	          "<svg role='img' class='icon--43-24 icon color-text'>
	              <use xlink:href='icons/icons.svg#icon-arrow-right-big'></use>
	            </svg>"
	        ]
	});

	//Base slider
	var btnNext = $('.js-next');
	var btnPrev = $('.js-prev');

	var baseSlider = $('.slider--base');
	var noSlider = $('.no-slider').length;

	if(!noSlider) {

		baseSlider.bxSlider({
	        pager: false,
	        mode: 'fade',
	        speed: 1000,
	        controls: false,
	        auto: true,
	        speed: 2200,

	        // onSliderLoad: function (currentIndex){
	        //     $('.slider__index--current').text(currentIndex + 1);
	        // },

	        onSlideBefore: function ($slideElement, oldIndex, newIndex){
	            $('.slider__index-current').text(newIndex + 1);

	        }

		});

		$('.slider__index-total').html(baseSlider.getSlideCount());

	//Small mobile slider
	var smallSlider = $('.slider--small');

	smallSlider.bxSlider({
        pager: false,
        mode: 'fade',
        controls: false,
        speed: 1000,
        auto: true,
        speed: 2200,
	});

		btnNext.on('click', function(){
			smallSlider.goToNextSlide();
			baseSlider.goToNextSlide();
		});

		btnPrev.on('click', function(){
			smallSlider.goToPrevSlide();
			baseSlider.goToPrevSlide();
		});
	} //end if (!noSlider)

	// function bigNext() {
	// 		baseSlider.goToNextSlide();
	// }

	// function bigPrev() {
	// 		baseSlider.goToPrevSlide();

	// }


	//Set image to bgr
	setBgr('bgr-wrap');

	//Activate info
	$('.js-info-trigger').on('click', function(){
		$(this).siblings('.slider__info').toggleClass('is-active');
	});

	//Show map
	$('.js-map-trigger').on('click', function(){
		$('body').addClass('map-is-active');
	});
	//Close map
	$('.js-close-map').on('click', function(){
		$('body').removeClass('map-is-active');
	});

	//Show contact
	$('.js-contact').on('click', function(){
		$('body').addClass('contact-is-active');
		$('body').removeClass('menu-is-active');
	});
	//Close contact
	$('.js-close-contact').on('click', function(){
		$('body').removeClass('contact-is-active');
	});

	//Load map

	if($('.home-map').length) {
		var locations = [
		      ['Bondi Beach', -33.890542, 151.274856, 4],
		      ['Coogee Beach', -33.923036, 151.259052, 5],
		      ['Cronulla Beach', -34.028249, 151.157507, 3],
		      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
		      ['Maroubra Beach', -33.950198, 151.259302, 1]
		    ];

		    var map = new google.maps.Map(document.getElementById('map'), {
		      zoom: 10,
		      scrollwheel: false,
		      center: new google.maps.LatLng(-33.92, 151.25),
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    });


		    var marker, i;

		    for (i = 0; i < locations.length; i++) {
		      marker = new google.maps.Marker({
		        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		        map: map,
		        icon: 'images/pin.png'
		      });
		    }
	}


	    //on resize run function
	    var tOut = false;
	    var milSec = 500;
	    $(window).resize(function(){
	     if(tOut !== false)
	        clearTimeout(tOut);
	     tOut = setTimeout(rsizeItems, milSec);
	    });
	    function rsizeItems()
	    {
	       //Full screen

	         	// alert(windowWidth);

	         	// if(windowWidth > 1200) {

	       		// fullHeight('full-screen');
	       		// fullHeight('item');
	       		// fullHeight('full__content .container');

	         	// }

	    }

	    //detect window resize
	    $(window).on('resize', function() {
	      //put scripts inside

	    }).trigger('resize');

	//Document on scroll
	$(document).scroll(function(){

	});

});

//WINDOW ONLOAD
$(window).load(function() {



  // WINDOW RESIZE
  $(window).on('resize', function() {

     });
}); //End load










